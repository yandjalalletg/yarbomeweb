import React from 'react';
import { Link } from 'react-router-dom';
import { Login } from '../Login/index';
import { ErrorMessage, Formik } from 'formik';
import * as Yup from 'yup';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import styled from 'styled-components/macro';

export function Signup() {
    return (
        <Container style={{ marginTop: '50px', marginBottom: '50px', fontFamily: 'Arial Rounded MT Bold' }}>
            <Row>
                <h2 className="text-uppercase mx-auto" style={{ marginBottom: '20px' }}>S'enregistrer</h2>
            </Row>
            <Row>
                <Col md={{offset:1, span: 10}}>
                    <CARD text="white" primary body >
                        <Formik
                            initialValues={{ Prenom: '', Nom: '', Email: '', Password: '', Confirmation: '', Telephone: ''}}
                            validationSchema={Yup.object({
                              Prenom: Yup.string().required(),
                              Nom: Yup.string().required(),
                              Email: Yup.string().email('Adresse e-mail invalide').required('Adresse e-mail requise'),
                              Password: Yup.string()
                                    .min(6, 'Mot de passe trop court, 6 charactères requis au minimum')
                                    .required('Mot de passe requis'),
                              Confirmation: Yup.string().required(),
                              Telephone: Yup.number().required(),
                            })}
                            onSubmit={(values, actions) => {
                                console.log(JSON.stringify(values, null, 2));
                                actions.setSubmitting(false);
                            }}
                        >
                          {formik =>(
                           <Form onSubmit={formik.handleSubmit}>
                              <Form.Group as={Row}>
                                        <Form.Label column='lg' md={3}>Prénom</Form.Label>
                                        <Col md={9} lg={6}>
                                            <FormControl
                                                id='Prenom'
                                                type='text'
                                                style={{ borderRadius: '20px' }}
                                                size='lg'
                                                placeholder='Prenom'
                                                {...formik.getFieldProps('Prenom')}
                                            />
                                            <span style={{ color: '#F063B8' }}>
                                                <ErrorMessage name='Prenom' />
                                            </span>
                                        </Col>
                                    </Form.Group>
                                    <Form.Group as={Row}>
                                        <Form.Label column='lg' md={3}>Nom</Form.Label>
                                        <Col md={9} lg={6}>
                                            <FormControl
                                                id='Nom'
                                                type='text'
                                                style={{ borderRadius: '20px' }}
                                                size='lg'
                                                placeholder='Nom'
                                                {...formik.getFieldProps('Nom')}
                                            />
                                            <span style={{ color: '#F063B8' }}>
                                                <ErrorMessage name='Nom' />
                                            </span>
                                        </Col>
                                    </Form.Group>
                                    <Form.Group as={Row}>
                                        <Form.Label column='lg' md={3}>Email</Form.Label>
                                        <Col md={9} lg={6}>
                                            <FormControl
                                                id='Email'
                                                type='email'
                                                style={{ borderRadius: '20px' }}
                                                size='lg'
                                                placeholder='Veuillez saisir votre adresse email'
                                                {...formik.getFieldProps('Email')}
                                            />
                                            <span style={{ color: '#F063B8' }}>
                                                <ErrorMessage name='Email' />
                                            </span>
                                        </Col>
                                    </Form.Group>
                                    <Form.Group as={Row}>
                                        <Form.Label column='lg' md={3}>Mot de passe</Form.Label>
                                        <Col md={9} lg={6}>
                                            <FormControl
                                                id='Password'
                                                type='password'
                                                style={{ borderRadius: '20px' }}
                                                size='lg'
                                                placeholder='Veuillez saisir votre mot de passe'
                                                {...formik.getFieldProps('Password')}
                                            />
                                            <span style={{ color: '#F063B8' }}>
                                                <ErrorMessage name='Password' />
                                            </span>
                                        </Col>
                                    </Form.Group>
                                    <Form.Group as={Row}>
                                        <Form.Label column='lg' md={3}>Confirmer Mot de Passe</Form.Label>
                                        <Col md={9} lg={6}>
                                            <FormControl
                                                id='Confirmation'
                                                type='password'
                                                style={{ borderRadius: '20px' }}
                                                size='lg'
                                                placeholder='Veuillez confirmer votre mot de passe'
                                                {...formik.getFieldProps('Confirmation')}
                                            />
                                            <span style={{ color: '#F063B8' }}>
                                                <ErrorMessage name='Confirmation' />
                                            </span>
                                        </Col>
                                    </Form.Group>
                                    <Form.Group as={Row}>
                                        <Form.Label column='lg' md={3}>Téléphone</Form.Label>
                                        <Col md={9} lg={6}>
                                            <FormControl
                                                id='Telephone'
                                                type='number'
                                                style={{ borderRadius: '20px' }}
                                                size='lg'
                                                placeholder='Veuillez saisir votre numéro de Téléphone'
                                                {...formik.getFieldProps('Telephone')}
                                            />
                                            <span style={{ color: '#F063B8' }}>
                                                <ErrorMessage name='Telephone' />
                                            </span>
                                        </Col>
                                    </Form.Group>

                                    <Form.Group as={Row} >
                                        <Col md={{ offset: 3 }}>
                                            <BUTTON type = 'button' variant='info' size='lg' style={{ borderRadius: '50px', marginRight: '10px' }} component={Login} to= "/login">
                                            <Link to="/login">Se Connecter</Link>
                                                
                                            </BUTTON>
                                            <BUTTON type = "submit" variant="light" size='lg' style={{ borderRadius: '50px' }}>S'enregistrer</BUTTON>
                                        </Col>
                                    </Form.Group>

                           </Form>
                          )}
                        </Formik>

                    </CARD>
                </Col>
            </Row>
        </Container>
    );
}
const BUTTON = styled(Button)`
  border-radius: 50px;
`;

const CARD = styled(Card)`
  border-radius: 50px;
  padding: 25px;
  background: ${props => (props.primary ? '#212353' : 'white')};
`;

const FormControl = styled(Form.Control)`
  border-radius: 25px;
`;
