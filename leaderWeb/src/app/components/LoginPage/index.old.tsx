import * as React from 'react';
import { Helmet } from 'react-helmet-async';
import styled from 'styled-components/macro';
import { Login } from '../Login/Loadable';

export function LoginPage() {
  return (
    <>
      <Helmet>
        <title>Login </title>
        <meta name="description" content="Create an freelance account here" />
      </Helmet>
      <Wrapper>
        <Title>Login</Title>
        <FormWrapper>
          <Login />
        </FormWrapper>
      </Wrapper>
    </>
  );
}

const Wrapper = styled.div`
  height: 100vh;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  min-height: 320px;
`;

const FormWrapper = styled.div`
  height: 100vh;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  min-height: 320px;
`;

const Title = styled.div`
  margin-top: -8vh;
  font-weight: bold;
  color: black;
  font-size: 3.375rem;

  span {
    font-size: 3.125rem;
  }
`;
