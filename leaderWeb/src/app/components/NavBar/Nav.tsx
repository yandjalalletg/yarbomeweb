import React from 'react';
import { useLocation } from 'react-router-dom';
import { NavList, LinkStyled, Wrapper } from './Nav.styled';

const LINKS = [
  { to: '/', text: 'Home' },
  { to: '/about', text: 'About' },
  { to: '/help', text: 'Help' },
  { to: '/feature', text: 'Features' },
  { to: '/signup', text: 'Signup' },
];

export function Nav() {
  const location = useLocation();

  return (
    <Wrapper>
      <NavList>
        {LINKS.map(item => (
          <li key={item.to}>
            <LinkStyled
              to={item.to}
              className={item.to === location.pathname ? 'active' : ''}
            >
              {item.text}
            </LinkStyled>
          </li>
        ))}
      </NavList>
    </Wrapper>
  );
}
