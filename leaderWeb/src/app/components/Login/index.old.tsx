import React from 'react';
import { ErrorMessage, Formik } from 'formik';
import * as Yup from 'yup';

import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';

export function Login() {
  return (
    <Container
      style={{ marginTop: '50px', fontFamily: 'Arial Rounded MT Bold' }}
    >
      <Row>
        <h2 className="text-uppercase mx-auto" style={{ marginBottom: '20px' }}>
          Se Connecter
        </h2>
      </Row>
      <Row>
        <Col md={{ offset: 1, span: 10 }}>
          <Card
            bg="primary"
            text="white"
            style={{
              borderRadius: '50px',
              paddingTop: '75px',
              paddingBottom: '75px',
            }}
            body
          >
            <Formik
              initialValues={{ userEmail: '', userPassword: '' }}
              validationSchema={Yup.object({
                userEmail: Yup.string()
                  .email('Adresse e-mail invalide')
                  .required('Adresse e-mail requise'),
                userPassword: Yup.string()
                  .min(
                    6,
                    'Mot de passe trop court, 6 charactères requis au minimum',
                  )
                  .required('Mot de passe requis'),
              })}
              onSubmit={(values, actions) => {
                console.log(JSON.stringify(values, null, 2));
                actions.setSubmitting(false);
              }}
            >
              {formik => (
                <Form onSubmit={formik.handleSubmit}>
                  <Form.Group as={Row}>
                    <Form.Label column="lg" md={3}>
                      Email
                    </Form.Label>
                    <Col md={9} lg={6}>
                      <Form.Control
                        id="userEmail"
                        type="email"
                        style={{ borderRadius: '20px' }}
                        size="lg"
                        placeholder="Veuillez saisir votre adresse email"
                        {...formik.getFieldProps('userEmail')}
                      />
                      <span style={{ color: '#F063B8' }}>
                        <ErrorMessage name="userEmail" />
                      </span>
                    </Col>
                  </Form.Group>
                  <Form.Group as={Row}>
                    <Form.Label column="lg" md={3}>
                      Mot de passe
                    </Form.Label>
                    <Col md={9} lg={6}>
                      <Form.Control
                        id="userPassword"
                        type="password"
                        style={{ borderRadius: '20px' }}
                        size="lg"
                        placeholder="Veuillez saisir votre mot de passe"
                        {...formik.getFieldProps('userPassword')}
                      />
                      <Form.Text id="passwordHelpBlock" muted>
                        Le mot de passe doit comprendre entre 6 à 20 charactères
                        alphanumériques.
                      </Form.Text>
                      <span style={{ color: '#F063B8' }}>
                        <ErrorMessage name="userPassword" />
                      </span>
                    </Col>
                  </Form.Group>
                  <Form.Group as={Row}>
                    <Col md={{ offset: 3 }}>
                      <Button
                        type="submit"
                        variant="info"
                        size="lg"
                        style={{ borderRadius: '50px', marginRight: '10px' }}
                      >
                        Se Connecter
                      </Button>
                      <Button
                        variant="light"
                        size="lg"
                        style={{ borderRadius: '50px' }}
                      >
                        S'enregistrer
                      </Button>
                    </Col>
                  </Form.Group>
                </Form>
              )}
            </Formik>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}
