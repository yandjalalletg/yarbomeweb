import React from 'react';
import { ErrorMessage, Formik } from 'formik';
import * as Yup from 'yup';
import { Link } from 'react-router-dom';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import styled from 'styled-components/macro';

export function Login() {
  return (
    <Container>
      <Row>
        <Col lg={{ offset: 1, span: 10 }}>
          <CARD text="white" primary body>
            <Formik
              initialValues={{ userEmail: '', userPassword: '' }}
              validationSchema={Yup.object({
                userEmail: Yup.string()
                  .email('Adresse e-mail invalide')
                  .required('Adresse e-mail requise'),
                userPassword: Yup.string()
                  .min(
                    6,
                    'Mot de passe trop court, 6 charactères requis au minimum',
                  )
                  .required('Mot de passe requis'),
              })}
              onSubmit={(values, actions) => {
                console.log(JSON.stringify(values, null, 2));
                actions.setSubmitting(false);
              }}
            >
              {formik => (
                <Form onSubmit={formik.handleSubmit}>
                  <Form.Group as={Row}>
                    <Form.Label column="lg" md={3}>
                      Email
                    </Form.Label>
                    <Col md={9} lg={9}>
                      <FormControl
                        id="userEmail"
                        type="email"
                        placeholder="Veuillez saisir votre adresse email"
                        {...formik.getFieldProps('userEmail')}
                      />
                      <span style={{ color: '#F063B8' }}>
                        <ErrorMessage name="userEmail" />
                      </span>
                    </Col>
                  </Form.Group>
                  <Form.Group as={Row}>
                    <Form.Label column="lg" md={3}>
                      Mot de passe
                    </Form.Label>
                    <Col md={9} lg={9}>
                      <FormControl
                        id="userPassword"
                        type="password"
                        placeholder="Veuillez saisir votre mot de passe"
                        {...formik.getFieldProps('userPassword')}
                      />
                      <Form.Text id="passwordHelpBlock" muted>
                        Le mot de passe doit comprendre entre 6 à 20 charactères
                        alphanumériques.
                      </Form.Text>
                      <span style={{ color: '#F063B8' }}>
                        <ErrorMessage name="userPassword" />
                      </span>
                    </Col>
                  </Form.Group>
                  <Form.Group as={Row}>
                    <Col md={{ offset: 3 }}>
                      <BUTTON type="submit"> Se Connecter </BUTTON>
                      <BUTTON type = 'button' variant="secondary"> <Link to="/signup">S'enregistrer </Link></BUTTON>
                    </Col>
                  </Form.Group>
                </Form>
              )}
            </Formik>
          </CARD>
        </Col>
      </Row>
    </Container>
  );
}

const BUTTON = styled(Button)`
  border-radius: 50px;
`;

const CARD = styled(Card)`
  border-radius: 50px;
  padding: 25px;
  background: ${props => (props.primary ? '#212353' : 'white')};
`;

const FormControl = styled(Form.Control)`
  border-radius: 25px;
`;
